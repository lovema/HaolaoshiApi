﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Common.Util;
using Web.Filter;

namespace Web.Teach.Controllers
{

    [Route("api/teach/[controller]/[action]")]
    [ApiController]
    [Authorize("teacher")]
    [QueryFilter]
    public class CourseController : MyBaseController<Course>
    {
        ICourseBll bll;
        public ICourseClassBll courseClassBll { get; set; }
        public ITeacherBll teacherBll { get; set; }
        public CourseController(ICourseBll bll)
        {
            this.bll = bll;
        }
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(bll.Query(where));
        }
        [HttpGet("{id}")]
        public Result Get([FromQuery] Dictionary<string, string> where)
        {
            return Result.Success("succeed").SetData(bll.SelectOne(where));
        }
        [HttpPost]
        public Result Add(Course o)
        {
            o.Sys = false;         
            return ModelState.IsValid ? (bll.Add(o) ? Result.Success("添加成功") : Result.Error("添加失败")) : Result.Error("添加失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }

        [HttpPost]
        public Result Update(Course o)
        {
            o.Sys = false;
            //if (string.IsNullOrEmpty(o.Intro) && !string.IsNullOrEmpty(o.Detail)) o.Intro = o.Detail.FilterHtmlExcept("p").HTMLSubstring(CourseIntroLen, "");
            return ModelState.IsValid ? (bll.Update(o) ? Result.Success("修改成功") : Result.Error("修改失败")) : Result.Error("修改失败!" + ModelState.GetAllErrMsgStr(";")); ;
        }
        // Get: api/Course/Delet/5
        [HttpGet("{id}")]
        public Result Delete([FromQuery] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpPost]
        public Result BatchDelete([FromForm] Dictionary<string, string> where)
        {
            return bll.Delete(where) ? Result.Success("删除成功") : Result.Error("删除失败");
        }
        [HttpGet]
        public Result QuerySysCourse()
        {            
            return Result.Success("succeed").SetData(bll.SelectAll(o => o.Sys == true));
        }
    }
}
