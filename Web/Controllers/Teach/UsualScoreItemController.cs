﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Bll;
using Common;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
using System;
using Model;

namespace Web.Teach.Controllers
{
    [Route("api/teach/[controller]/[action]")]
    [ApiController]
    [Authorize("teacher")]
    [QueryFilter]
    public class UsualScoreItemController : MyBaseController<UsualScoreItem>
    {
        IUsualScoreItemBll bll;
        public UsualScoreItemController(IUsualScoreItemBll bll)
        {
            this.bll = bll;
        }
        // GET: api/List/UsualScoreItem
        [HttpGet]
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            //return Result.Success("succeed").SetData(bll.SelectAll(o => true, pageNo, pageSize));
            return Result.Success("succeed").SetData(bll.Query(where));
        }        
    }
}
