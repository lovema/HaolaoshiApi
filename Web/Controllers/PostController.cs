﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
namespace Web.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
   // [IdentityModelActionFilter]
    public class PostController : MyBaseController<Post>
    {
        IPostBll bll;
        public IInterviewBll interviewBll { get; set; }
        public PostController(IPostBll bll)
        {
            this.bll = bll;
        }
        // GET: api/stu/post/List
        [HttpGet]
        [ResponseCache(Duration = 60, VaryByQueryKeys = new string[] { PageParam.pageNo })]//https://www.pianshen.com/article/24171627661/
        public Result List([FromQuery] Dictionary<string, string> where)
        {
            where.Remove("TeacherId");
            where.Remove("StudentId");
            where.Remove("UserId");
            where.Add("sort", "-Id");
            return Result.Success("succeed").SetData(bll.Query(where));
        }

        // GET: api/stu/post/get/5
        [HttpGet("{id}")]
        public Result Get(int id )
        {         
            return Result.Success("succeed").SetData(bll.SelectOne(id));
        }
        /// <summary>
        /// 当前职位下的面试记录
        /// </summary>
        /// <param name="where">条件。PostId条件必填</param>
        /// <returns></returns>
        [HttpGet]
        public Result Interviews([FromQuery] Dictionary<string, string> where)
        {
            where.Remove("TeacherId");
            where.Remove("StudentId");
            where.Remove("UserId"); 
            where.Add("sort", "-Id");
            if (where["PostId"] ==null)
            {
                return Result.Error("PostId不能为空");
            }
            return Result.Success("succeed").SetData(interviewBll.Query(where));
        }
    }
}
