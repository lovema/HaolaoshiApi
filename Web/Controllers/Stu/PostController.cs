﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Bll;
using Common;
using Web.Extension;
using Microsoft.AspNetCore.Authorization;
using Web.Controllers;
using Web.Filter;
namespace Web.Controllers.Stu
{
    [Route("api/stu/[controller]/[action]")]
    [ApiController]
    [Authorize("student")]
    [QueryFilter]
    public class PostController : MyBaseController<Post>
    {
        IPostBll bll;
        public IInterviewBll interviewBll { get; set; }
        public PostController(IPostBll bll)
        {
            this.bll = bll;
        }
        /// <summary>
        /// 职位申请
        /// </summary>
        /// <param name="postid"></param>
        /// <returns></returns>
        [HttpPost]
        public Result Apply(int postid)
        {
            //判断该职位是否已经结束报名
            Post post = bll.SelectOne(t => t.Id == postid);
            if (post == null || post.Interview_time != null && post.Interview_time < DateTime.Now)
            {
                return Result.Error("报名失败，该职位报名结束");
            }
            //学生是否应聘某职位
            Interview interview = interviewBll.SelectOne(t => t.PostId == postid && t.StudentId == MyUser.Id);
            if (interview != null)
            {
                return Result.Error("报名失败，您已经报名");
            }                 
            interview = new Interview() { Post = post, StudentId = MyUser.Id , Sys =false};
            interviewBll.Add(interview);
            return Result.Success("报名成功");
        }
    }
}
