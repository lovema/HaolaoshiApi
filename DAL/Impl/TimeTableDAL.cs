using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class TimeTableDAL : BaseDAL<TimeTable>, ITimeTableDAL
    {
        public TimeTableDAL(MyDbContext db) : base(db)
        {
        }
    }
}
