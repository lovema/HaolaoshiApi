using System;
using System.Collections.Generic;
using System.Text;
using Model;
namespace DAL.Impl
{

    public class CollegeDAL : BaseDAL<College>, ICollegeDAL
    {
        public CollegeDAL(MyDbContext db) : base(db)
        {
        }
    }
}
