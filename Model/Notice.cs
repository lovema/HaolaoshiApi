﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /**
     * 公告通知
     */
    [Serializable]
    [Table("Notice")]
    public class Notice : UserID
    {

        [Display(Name = "名称")]
        [Required(ErrorMessage = "名称必填")]
        public string Name { get; set; }
        /**
         * 文本
         */
        public string Text { get; set; }      
      /// <summary>
      /// 跳转地址
      /// </summary>
        public string Url { get; set; }
    }
}