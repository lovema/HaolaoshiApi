﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    /// <summary>
    /// 付费
    /// </summary>
   public interface IFee
    {
        public int Gold { get; set; }
    }
}
