﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi38 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Authority_Student_StudentId",
                table: "Authority");

            migrationBuilder.DropForeignKey(
                name: "FK_Authority_Teacher_TeacherId",
                table: "Authority");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorityRes_Student_StudentId",
                table: "AuthorityRes");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorityRes_Teacher_TeacherId",
                table: "AuthorityRes");

            migrationBuilder.DropForeignKey(
                name: "FK_Category_Student_StudentId",
                table: "Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Category_Teacher_TeacherId",
                table: "Category");

            migrationBuilder.DropForeignKey(
                name: "FK_Classroom_Student_StudentId",
                table: "Classroom");

            migrationBuilder.DropForeignKey(
                name: "FK_Classroom_Teacher_TeacherId",
                table: "Classroom");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassSchedule_Student_StudentId",
                table: "ClassSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_Clock_Student_StudentId",
                table: "Clock");

            migrationBuilder.DropForeignKey(
                name: "FK_Clock_Teacher_TeacherId",
                table: "Clock");

            migrationBuilder.DropForeignKey(
                name: "FK_ClockLog_Teacher_TeacherId",
                table: "ClockLog");

            migrationBuilder.DropForeignKey(
                name: "FK_College_Student_StudentId",
                table: "College");

            migrationBuilder.DropForeignKey(
                name: "FK_College_Teacher_TeacherId",
                table: "College");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Student_StudentId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Teacher_TeacherId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_Course_Student_StudentId",
                table: "Course");

            migrationBuilder.DropForeignKey(
                name: "FK_Grade_Student_StudentId",
                table: "Grade");

            migrationBuilder.DropForeignKey(
                name: "FK_Grade_Teacher_TeacherId",
                table: "Grade");

            migrationBuilder.DropForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview");

            migrationBuilder.DropForeignKey(
                name: "FK_Major_Student_StudentId",
                table: "Major");

            migrationBuilder.DropForeignKey(
                name: "FK_Major_Teacher_TeacherId",
                table: "Major");

            migrationBuilder.DropForeignKey(
                name: "FK_OnlineResume_Student_StudentId",
                table: "OnlineResume");

            migrationBuilder.DropForeignKey(
                name: "FK_OnlineResume_Teacher_TeacherId",
                table: "OnlineResume");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_Student_StudentId",
                table: "Post");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_Teacher_TeacherId",
                table: "Post");

            migrationBuilder.DropForeignKey(
                name: "FK_Res_Student_StudentId",
                table: "Res");

            migrationBuilder.DropForeignKey(
                name: "FK_Res_Teacher_TeacherId",
                table: "Res");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_Student_StudentId",
                table: "Role");

            migrationBuilder.DropForeignKey(
                name: "FK_Role_Teacher_TeacherId",
                table: "Role");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleAuthority_Student_StudentId",
                table: "RoleAuthority");

            migrationBuilder.DropForeignKey(
                name: "FK_RoleAuthority_Teacher_TeacherId",
                table: "RoleAuthority");

            migrationBuilder.DropForeignKey(
                name: "FK_Score_Teacher_TeacherId",
                table: "Score");

            migrationBuilder.DropForeignKey(
                name: "FK_Slider_Student_StudentId",
                table: "Slider");

            migrationBuilder.DropForeignKey(
                name: "FK_Slider_Teacher_TeacherId",
                table: "Slider");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeTable_Student_StudentId",
                table: "TimeTable");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeTable_Teacher_TeacherId",
                table: "TimeTable");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScore_Teacher_TeacherId",
                table: "UsualScore");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreItem_Teacher_TeacherId",
                table: "UsualScoreItem");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreLog_Teacher_TeacherId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreLog_TeacherId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreItem_TeacherId",
                table: "UsualScoreItem");

            migrationBuilder.DropIndex(
                name: "IX_UsualScore_TeacherId",
                table: "UsualScore");

            migrationBuilder.DropIndex(
                name: "IX_TimeTable_StudentId",
                table: "TimeTable");

            migrationBuilder.DropIndex(
                name: "IX_TimeTable_TeacherId",
                table: "TimeTable");

            migrationBuilder.DropIndex(
                name: "IX_Slider_StudentId",
                table: "Slider");

            migrationBuilder.DropIndex(
                name: "IX_Slider_TeacherId",
                table: "Slider");

            migrationBuilder.DropIndex(
                name: "IX_Score_TeacherId",
                table: "Score");

            migrationBuilder.DropIndex(
                name: "IX_RoleAuthority_StudentId",
                table: "RoleAuthority");

            migrationBuilder.DropIndex(
                name: "IX_RoleAuthority_TeacherId",
                table: "RoleAuthority");

            migrationBuilder.DropIndex(
                name: "IX_Role_StudentId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Role_TeacherId",
                table: "Role");

            migrationBuilder.DropIndex(
                name: "IX_Res_StudentId",
                table: "Res");

            migrationBuilder.DropIndex(
                name: "IX_Res_TeacherId",
                table: "Res");

            migrationBuilder.DropIndex(
                name: "IX_Post_StudentId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_Post_TeacherId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_OnlineResume_StudentId",
                table: "OnlineResume");

            migrationBuilder.DropIndex(
                name: "IX_OnlineResume_TeacherId",
                table: "OnlineResume");

            migrationBuilder.DropIndex(
                name: "IX_Major_StudentId",
                table: "Major");

            migrationBuilder.DropIndex(
                name: "IX_Major_TeacherId",
                table: "Major");

            migrationBuilder.DropIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Grade_StudentId",
                table: "Grade");

            migrationBuilder.DropIndex(
                name: "IX_Grade_TeacherId",
                table: "Grade");

            migrationBuilder.DropIndex(
                name: "IX_Course_StudentId",
                table: "Course");

            migrationBuilder.DropIndex(
                name: "IX_Company_StudentId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_Company_TeacherId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_College_StudentId",
                table: "College");

            migrationBuilder.DropIndex(
                name: "IX_College_TeacherId",
                table: "College");

            migrationBuilder.DropIndex(
                name: "IX_ClockLog_TeacherId",
                table: "ClockLog");

            migrationBuilder.DropIndex(
                name: "IX_Clock_StudentId",
                table: "Clock");

            migrationBuilder.DropIndex(
                name: "IX_Clock_TeacherId",
                table: "Clock");

            migrationBuilder.DropIndex(
                name: "IX_ClassSchedule_StudentId",
                table: "ClassSchedule");

            migrationBuilder.DropIndex(
                name: "IX_Classroom_StudentId",
                table: "Classroom");

            migrationBuilder.DropIndex(
                name: "IX_Classroom_TeacherId",
                table: "Classroom");

            migrationBuilder.DropIndex(
                name: "IX_Category_StudentId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_Category_TeacherId",
                table: "Category");

            migrationBuilder.DropIndex(
                name: "IX_AuthorityRes_StudentId",
                table: "AuthorityRes");

            migrationBuilder.DropIndex(
                name: "IX_AuthorityRes_TeacherId",
                table: "AuthorityRes");

            migrationBuilder.DropIndex(
                name: "IX_Authority_StudentId",
                table: "Authority");

            migrationBuilder.DropIndex(
                name: "IX_Authority_TeacherId",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "RoleAuthority");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Role");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Res");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "College");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "College");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "ClockLog");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "AuthorityRes");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Authority");

            migrationBuilder.DropColumn(
                name: "TeacherId",
                table: "Authority");

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "UsualScoreLog",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "UsualScoreItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "UsualScore",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "TimeTable",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Slider",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Score",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Post",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "OnlineResume",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Major",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Interview",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Images",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Group",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Grade",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "CourseRes",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "ClockLog",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Clock",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "ClassSchedule",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Classs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Classroom",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "Article",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Area",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreLog_SchoolId",
                table: "UsualScoreLog",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreItem_SchoolId",
                table: "UsualScoreItem",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScore_SchoolId",
                table: "UsualScore",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_SchoolId",
                table: "TimeTable",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_SchoolId",
                table: "Slider",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Score_SchoolId",
                table: "Score",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_SchoolId",
                table: "Post",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_OnlineResume_SchoolId",
                table: "OnlineResume",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Major_SchoolId",
                table: "Major",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Interview_SchoolId",
                table: "Interview",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Images_SchoolId",
                table: "Images",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Group_SchoolId",
                table: "Group",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Grade_SchoolId",
                table: "Grade",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_CourseRes_SchoolId",
                table: "CourseRes",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_SchoolId",
                table: "Company",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_ClockLog_SchoolId",
                table: "ClockLog",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Clock_SchoolId",
                table: "Clock",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSchedule_SchoolId",
                table: "ClassSchedule",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Classs_SchoolId",
                table: "Classs",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Classroom_SchoolId",
                table: "Classroom",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Article_SchoolId",
                table: "Article",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Area_UserId",
                table: "Area",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_User_UserId",
                table: "Area",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Article_School_SchoolId",
                table: "Article",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classroom_School_SchoolId",
                table: "Classroom",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classs_School_SchoolId",
                table: "Classs",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassSchedule_School_SchoolId",
                table: "ClassSchedule",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Clock_School_SchoolId",
                table: "Clock",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClockLog_School_SchoolId",
                table: "ClockLog",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_School_SchoolId",
                table: "Company",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CourseRes_School_SchoolId",
                table: "CourseRes",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grade_School_SchoolId",
                table: "Grade",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Group_School_SchoolId",
                table: "Group",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Images_School_SchoolId",
                table: "Images",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_School_SchoolId",
                table: "Interview",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Major_School_SchoolId",
                table: "Major",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OnlineResume_School_SchoolId",
                table: "OnlineResume",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_School_SchoolId",
                table: "Post",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Score_School_SchoolId",
                table: "Score",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Slider_School_SchoolId",
                table: "Slider",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeTable_School_SchoolId",
                table: "TimeTable",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScore_School_SchoolId",
                table: "UsualScore",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreItem_School_SchoolId",
                table: "UsualScoreItem",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreLog_School_SchoolId",
                table: "UsualScoreLog",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_User_UserId",
                table: "Area");

            migrationBuilder.DropForeignKey(
                name: "FK_Article_School_SchoolId",
                table: "Article");

            migrationBuilder.DropForeignKey(
                name: "FK_Classroom_School_SchoolId",
                table: "Classroom");

            migrationBuilder.DropForeignKey(
                name: "FK_Classs_School_SchoolId",
                table: "Classs");

            migrationBuilder.DropForeignKey(
                name: "FK_ClassSchedule_School_SchoolId",
                table: "ClassSchedule");

            migrationBuilder.DropForeignKey(
                name: "FK_Clock_School_SchoolId",
                table: "Clock");

            migrationBuilder.DropForeignKey(
                name: "FK_ClockLog_School_SchoolId",
                table: "ClockLog");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_School_SchoolId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_CourseRes_School_SchoolId",
                table: "CourseRes");

            migrationBuilder.DropForeignKey(
                name: "FK_Grade_School_SchoolId",
                table: "Grade");

            migrationBuilder.DropForeignKey(
                name: "FK_Group_School_SchoolId",
                table: "Group");

            migrationBuilder.DropForeignKey(
                name: "FK_Images_School_SchoolId",
                table: "Images");

            migrationBuilder.DropForeignKey(
                name: "FK_Interview_School_SchoolId",
                table: "Interview");

            migrationBuilder.DropForeignKey(
                name: "FK_Major_School_SchoolId",
                table: "Major");

            migrationBuilder.DropForeignKey(
                name: "FK_OnlineResume_School_SchoolId",
                table: "OnlineResume");

            migrationBuilder.DropForeignKey(
                name: "FK_Post_School_SchoolId",
                table: "Post");

            migrationBuilder.DropForeignKey(
                name: "FK_Score_School_SchoolId",
                table: "Score");

            migrationBuilder.DropForeignKey(
                name: "FK_Slider_School_SchoolId",
                table: "Slider");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeTable_School_SchoolId",
                table: "TimeTable");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScore_School_SchoolId",
                table: "UsualScore");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreItem_School_SchoolId",
                table: "UsualScoreItem");

            migrationBuilder.DropForeignKey(
                name: "FK_UsualScoreLog_School_SchoolId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreLog_SchoolId",
                table: "UsualScoreLog");

            migrationBuilder.DropIndex(
                name: "IX_UsualScoreItem_SchoolId",
                table: "UsualScoreItem");

            migrationBuilder.DropIndex(
                name: "IX_UsualScore_SchoolId",
                table: "UsualScore");

            migrationBuilder.DropIndex(
                name: "IX_TimeTable_SchoolId",
                table: "TimeTable");

            migrationBuilder.DropIndex(
                name: "IX_Slider_SchoolId",
                table: "Slider");

            migrationBuilder.DropIndex(
                name: "IX_Score_SchoolId",
                table: "Score");

            migrationBuilder.DropIndex(
                name: "IX_Post_SchoolId",
                table: "Post");

            migrationBuilder.DropIndex(
                name: "IX_OnlineResume_SchoolId",
                table: "OnlineResume");

            migrationBuilder.DropIndex(
                name: "IX_Major_SchoolId",
                table: "Major");

            migrationBuilder.DropIndex(
                name: "IX_Interview_SchoolId",
                table: "Interview");

            migrationBuilder.DropIndex(
                name: "IX_Images_SchoolId",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "IX_Group_SchoolId",
                table: "Group");

            migrationBuilder.DropIndex(
                name: "IX_Grade_SchoolId",
                table: "Grade");

            migrationBuilder.DropIndex(
                name: "IX_CourseRes_SchoolId",
                table: "CourseRes");

            migrationBuilder.DropIndex(
                name: "IX_Company_SchoolId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_ClockLog_SchoolId",
                table: "ClockLog");

            migrationBuilder.DropIndex(
                name: "IX_Clock_SchoolId",
                table: "Clock");

            migrationBuilder.DropIndex(
                name: "IX_ClassSchedule_SchoolId",
                table: "ClassSchedule");

            migrationBuilder.DropIndex(
                name: "IX_Classs_SchoolId",
                table: "Classs");

            migrationBuilder.DropIndex(
                name: "IX_Classroom_SchoolId",
                table: "Classroom");

            migrationBuilder.DropIndex(
                name: "IX_Article_SchoolId",
                table: "Article");

            migrationBuilder.DropIndex(
                name: "IX_Area_UserId",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "UsualScoreLog");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "UsualScoreItem");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "UsualScore");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "TimeTable");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Slider");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Score");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Post");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "OnlineResume");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Major");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Interview");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Group");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Grade");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "CourseRes");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "ClockLog");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Clock");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "ClassSchedule");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Classs");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Classroom");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "Article");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Area");

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "UsualScoreLog",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "UsualScoreItem",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "UsualScore",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "TimeTable",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "TimeTable",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Slider",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Slider",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Score",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "RoleAuthority",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "RoleAuthority",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Role",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Role",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Res",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Res",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Post",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Post",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "OnlineResume",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "OnlineResume",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Major",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Major",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Interview",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Grade",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Grade",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Course",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Company",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Company",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "College",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "College",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "ClockLog",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Clock",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Clock",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "ClassSchedule",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Classroom",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Classroom",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Category",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Category",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "AuthorityRes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "AuthorityRes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Authority",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId",
                table: "Authority",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreLog_TeacherId",
                table: "UsualScoreLog",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScoreItem_TeacherId",
                table: "UsualScoreItem",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_UsualScore_TeacherId",
                table: "UsualScore",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_StudentId",
                table: "TimeTable",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_TimeTable_TeacherId",
                table: "TimeTable",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_StudentId",
                table: "Slider",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Slider_TeacherId",
                table: "Slider",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Score_TeacherId",
                table: "Score",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleAuthority_StudentId",
                table: "RoleAuthority",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_RoleAuthority_TeacherId",
                table: "RoleAuthority",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_StudentId",
                table: "Role",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Role_TeacherId",
                table: "Role",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Res_StudentId",
                table: "Res",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Res_TeacherId",
                table: "Res",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_StudentId",
                table: "Post",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Post_TeacherId",
                table: "Post",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_OnlineResume_StudentId",
                table: "OnlineResume",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_OnlineResume_TeacherId",
                table: "OnlineResume",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Major_StudentId",
                table: "Major",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Major_TeacherId",
                table: "Major",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Interview_TeacherId",
                table: "Interview",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Grade_StudentId",
                table: "Grade",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Grade_TeacherId",
                table: "Grade",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Course_StudentId",
                table: "Course",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_StudentId",
                table: "Company",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_TeacherId",
                table: "Company",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_College_StudentId",
                table: "College",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_College_TeacherId",
                table: "College",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_ClockLog_TeacherId",
                table: "ClockLog",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Clock_StudentId",
                table: "Clock",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Clock_TeacherId",
                table: "Clock",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSchedule_StudentId",
                table: "ClassSchedule",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Classroom_StudentId",
                table: "Classroom",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Classroom_TeacherId",
                table: "Classroom",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_StudentId",
                table: "Category",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_TeacherId",
                table: "Category",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorityRes_StudentId",
                table: "AuthorityRes",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorityRes_TeacherId",
                table: "AuthorityRes",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Authority_StudentId",
                table: "Authority",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Authority_TeacherId",
                table: "Authority",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Authority_Student_StudentId",
                table: "Authority",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Authority_Teacher_TeacherId",
                table: "Authority",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorityRes_Student_StudentId",
                table: "AuthorityRes",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorityRes_Teacher_TeacherId",
                table: "AuthorityRes",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Category_Student_StudentId",
                table: "Category",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Category_Teacher_TeacherId",
                table: "Category",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classroom_Student_StudentId",
                table: "Classroom",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classroom_Teacher_TeacherId",
                table: "Classroom",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClassSchedule_Student_StudentId",
                table: "ClassSchedule",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Clock_Student_StudentId",
                table: "Clock",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Clock_Teacher_TeacherId",
                table: "Clock",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ClockLog_Teacher_TeacherId",
                table: "ClockLog",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_College_Student_StudentId",
                table: "College",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_College_Teacher_TeacherId",
                table: "College",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Student_StudentId",
                table: "Company",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Teacher_TeacherId",
                table: "Company",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Course_Student_StudentId",
                table: "Course",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grade_Student_StudentId",
                table: "Grade",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Grade_Teacher_TeacherId",
                table: "Grade",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Interview_Teacher_TeacherId",
                table: "Interview",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Major_Student_StudentId",
                table: "Major",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Major_Teacher_TeacherId",
                table: "Major",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OnlineResume_Student_StudentId",
                table: "OnlineResume",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OnlineResume_Teacher_TeacherId",
                table: "OnlineResume",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_Student_StudentId",
                table: "Post",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Post_Teacher_TeacherId",
                table: "Post",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Res_Student_StudentId",
                table: "Res",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Res_Teacher_TeacherId",
                table: "Res",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_Student_StudentId",
                table: "Role",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Role_Teacher_TeacherId",
                table: "Role",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleAuthority_Student_StudentId",
                table: "RoleAuthority",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoleAuthority_Teacher_TeacherId",
                table: "RoleAuthority",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Score_Teacher_TeacherId",
                table: "Score",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Slider_Student_StudentId",
                table: "Slider",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Slider_Teacher_TeacherId",
                table: "Slider",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeTable_Student_StudentId",
                table: "TimeTable",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeTable_Teacher_TeacherId",
                table: "TimeTable",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScore_Teacher_TeacherId",
                table: "UsualScore",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreItem_Teacher_TeacherId",
                table: "UsualScoreItem",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UsualScoreLog_Teacher_TeacherId",
                table: "UsualScoreLog",
                column: "TeacherId",
                principalTable: "Teacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
