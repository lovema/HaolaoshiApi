﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Credentials",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Credentials",
                table: "Teacher",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Credentials",
                table: "Student",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Credentials",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Credentials",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "Credentials",
                table: "Student");
        }
    }
}
