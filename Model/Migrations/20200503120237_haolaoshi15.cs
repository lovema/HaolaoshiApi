﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Model.Migrations
{
    public partial class haolaoshi15 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CollegeId",
                table: "College",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SchoolId",
                table: "College",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "School",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    added_time = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Pic = table.Column<string>(nullable: true),
                    Intro = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_School", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_College_SchoolId",
                table: "College",
                column: "SchoolId");

            migrationBuilder.AddForeignKey(
                name: "FK_College_School_SchoolId",
                table: "College",
                column: "SchoolId",
                principalTable: "School",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_College_School_SchoolId",
                table: "College");

            migrationBuilder.DropTable(
                name: "School");

            migrationBuilder.DropIndex(
                name: "IX_College_SchoolId",
                table: "College");

            migrationBuilder.DropColumn(
                name: "CollegeId",
                table: "College");

            migrationBuilder.DropColumn(
                name: "SchoolId",
                table: "College");
        }
    }
}
