﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    //面试记录
    [Serializable]
    [Table("Interview")]
    public class Interview : SchoolUserStudentID
    {
        /// <summary>
        /// 应聘状态
        /// </summary>
        public enum InterviewStatus
        {
            [Display(Name = "尚未面试")]
            NoInterviewed = 1,
            [Display(Name = "已面试")]
            Interviewed = 2,
            [Display(Name = "面试通过")]
            Passed = 3,
            [Display(Name = "面试未通过")]
            NoPassed = 4
        }
        /**
        * 签到类型
        */
        public InterviewStatus Status { get; set; } = InterviewStatus.NoInterviewed;
        /// <summary>
        /// 弃用
        /// </summary>
        [Display(Name = "是否已面试")]
        public bool Interviewed { get; set; }
        [Display(Name = "提问问题")]
        public string Ask { get; set; }
        /// <summary>
        /// 弃用
        /// </summary>
        [Display(Name = "面试结果")]
        public bool Result { get; set; }
        public int? PostId { get; set; }
        [ForeignKey("PostId")]
        [Display(Name = "应聘职位")]
        public virtual Post Post { get; set; }
    }
}