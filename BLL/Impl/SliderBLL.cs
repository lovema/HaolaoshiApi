using System;
using System.Collections.Generic;
using System.Text;
using Model;
using DAL;
namespace Bll.Impl
{
    public class SliderBll : BaseBll<Slider>, ISliderBll
    {
        public SliderBll(ISliderDAL dal):base(dal)
        {
        }
    }
}
